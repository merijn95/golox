package main

import "fmt"

const ErrUsage = 64
const ErrData = 65

var HadError bool

func ReportError(line int, message string) {
	reportError(line, "", message)
}

func reportError(line int, where, message string) {
	fmt.Printf("[line %d] error %s : %s\n", line, where, message)
	HadError = true
}