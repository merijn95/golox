package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func run(data string) {
	scanner := NewScanner(data)
	tokens, err := scanner.Scan()
	if err != nil {
		log.Fatalln("lexer error", err)
	}
	fmt.Println(tokens)
}

func runFile(file string) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		log.Println("io error", err)
		os.Exit(ErrData)
	}
	run(string(data))
	if HadError {
		os.Exit(ErrData)
	}
}

func runRepl() {
	for {
		fmt.Print("repl> ")
		var input string
		_, err := fmt.Scan(&input)
		if err != nil {
			log.Println("unable to read input")
			continue
		}
		run(input)
	}
}

func main() {
	// before anything just print the ast
	ast := &Group{
		&BinaryOp{
			Left: &LiteralExpr{Value: 10},
			Operator: Token{
				Lexeme:  "*",
				Type:    STAR,
				Literal: nil,
				Line:    0,
			},
			Right: &UnaryOp{
				Operator: Token{
					Lexeme:  "-",
					Type:    MINUS,
					Literal: nil,
					Line:    0,
				},
				Literal: &LiteralExpr{Value: 20},
			},
		},
	}

	printVisitor := &PrettyPrintVisitor{}

	fmt.Println(ast.Accept(printVisitor))

	if len(os.Args) > 2 {
		log.Println("usage: golox [script]")
		os.Exit(ErrUsage)
	} else if len(os.Args) == 2 {
		runFile(os.Args[1])
	} else {
		fmt.Println("golox programming language")
		runRepl()
	}
}

