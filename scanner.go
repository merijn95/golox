package main

import "strconv"

type TokenType int

type Token struct {
	Lexeme string
	Type TokenType
	Literal interface{}
	Line int
}

// reversed keywords
var keywords = map[string]TokenType{
	"and": And,
	"class": Class,
	"else": Else,
	"false": False,
	"for": For,
	"fun": Fun,
	"if": If,
	"nil": Nil,
	"or": Or,
	"print": Print,
	"return": Return,
	"super": Super,
	"this": This,
	"true": True,
	"var": Var,
	"while": While,
}

const (
	// Single character tokens
	LEFT_PAREN = iota + 1
	RIGHT_PAREN
	LEFT_BRACE
	RIGHT_BRACE
	COMMA
	DOT
	MINUS
	PLUS
	SEMICOLON
	SLASH
	STAR

	// One or two character tokens
	BANG
	BangEqual
	Equal
	EqualEqual
	Greater
	GreaterEqual
	Less
	LessEqual

	// Literals
	Identifier
	String
	Number

	// Keywords
	And
	Class
	Else
	False
	Fun
	For
	If
	Nil
	Or
	Print
	Return
	Super
	This
	True
	Var
	While
)

type Scanner struct {
	tokens []Token
	source  string
	start   int
	current int
	line    int
}

func NewScanner(source string) *Scanner {
	return &Scanner{
		tokens: []Token{},
		source:  source,
		start:   0,
		current: 0,
		line:    1,
	}
}

func (s *Scanner) Scan() ([]Token, error) {
	for !s.isAtEnd() {
		s.start = s.current
		c := s.advance()
		switch c {
		case '(': s.addToken(LEFT_PAREN, nil)
		break
		case ')': s.addToken(RIGHT_PAREN, nil)
		break
		case '{': s.addToken(LEFT_BRACE, nil)
		break
		case '}': s.addToken(RIGHT_BRACE, nil)
		break
		case ',': s.addToken(COMMA, nil)
		break
		case '-': s.addToken(MINUS, nil)
		break
		case '+': s.addToken(PLUS, nil)
		break
		case ';': s.addToken(SEMICOLON, nil)
		break
		case '*': s.addToken(STAR, nil)
		break
		case '!':
			if s.match('=') {
				s.addToken(BangEqual, nil)
			} else {
				s.addToken(BANG, nil)
			}
			break
		case '=':
			if s.match('=') {
				s.addToken(EqualEqual, nil)
			} else {
				s.addToken(Equal, nil)
			}
			break
		case '<':
			if s.match('=') {
				s.addToken(LessEqual, nil)
			} else {
				s.addToken(Less, nil)
			}
			break
		case '>':
			if s.match('=') {
				s.addToken(GreaterEqual, nil)
			} else {
				s.addToken(Greater, nil)
			}
			break
		case '/':
			if s.match('/') {
				// Continue until \n has been reached
				for s.peek() != '\n' && !s.isAtEnd() {
					_ = s.advance()
				}
			} else {
				s.addToken(SLASH, nil)
			}
			break
		case ' ':
			break
		case '\r':
			break
		case '\t':
			// ignore whitespace characters
			break
		case '\n':
			s.line++
			break
		case '"': s.string()
			break
		default:
			if s.isDigit(c) {
				s.number()
			} else if s.isAlpha(c) {
				s.identifier()
			} else {
				ReportError(s.line, "Unexpected character")
			}
			break
		}
	}

	// append EOF token

	return s.tokens, nil
}

func (s *Scanner) isAtEnd() bool {
	return s.current >= len(s.source)
}

func (s *Scanner) peek() byte {
	if s.isAtEnd() {
		return '\x00'
	}
	return s.source[s.current]
}

func (s *Scanner) advance() byte {
	s.current++
	return s.source[s.current-1]
}

func (s *Scanner) isDigit(c byte) bool {
	return c >= '0' && c <= '9'
}

func (s *Scanner) addToken(tokenType TokenType, literal interface{})  {
	text := s.source[s.start:s.current]
	token := Token {
		Type: tokenType,
		Lexeme: text,
		Literal: literal,
		Line: s.line,
	}
	s.tokens = append(s.tokens, token)
}

func (s *Scanner) match(expected byte) bool {
	if s.isAtEnd() {
		return false
	}
	if s.source[s.current] != expected {
		return false
	}
	s.current++
	return true
}

func (s *Scanner) string() {
	for s.peek() != '"' && !s.isAtEnd() {
		// increment s.line if we're going to the next line.
		if s.peek() == '\n' {
			s.line++
		}
		_ = s.advance()
	}

	// Handle unterminated strings.
	if s.isAtEnd() {
		ReportError(s.line, "Unterminated string.")
		return
	}
	_ = s.advance()

	// remove the quotes from the string
	value := s.source[s.start+1:s.current-1]

	// add value to the slice of tokens.
	s.addToken(String, value)
}

func (s *Scanner) number() {
	for s.isDigit(s.peek()) && !s.isAtEnd() {
		_ = s.advance()
	}

	// Consume the dot if the number is a fraction
	if s.peek() == '.' && s.isDigit(s.peekNext()) {
		s.advance()
		for s.isDigit(s.peek()) {
			_ = s.advance()
		}
	}

	number, _ := strconv.ParseFloat(s.source[s.start:s.current], 64)
	s.addToken(Number, number)
}

func (s *Scanner) peekNext() byte {
	if s.current + 1 >= len(s.source) {
		return '\x00'
	}
	return s.source[s.current+1]
}

func (s *Scanner) isAlpha(c byte) bool {
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_'
}

func (s *Scanner) isAlphaNumeric(c byte) bool {
	return s.isAlpha(c) || s.isDigit(c)
}

func (s *Scanner) identifier() {
	for s.isAlphaNumeric(s.peek()) {
		_ = s.advance()
	}
	tokenType, ok := keywords[s.source[s.start:s.current]]
	if !ok {
		s.addToken(Identifier, nil)
	} else {
		s.addToken(tokenType, nil)
	}
}