package main

import (
	"strconv"
	"strings"
)

type AstVisitor interface {
	VisitBinary(binary *BinaryOp) interface{}
	VisitLiteral(literal *LiteralExpr) interface{}
	VisitUnary(unary *UnaryOp) interface{}
	VisitGroup(group *Group) interface{}
}

type PrettyPrintVisitor struct {

}

func (p *PrettyPrintVisitor) VisitBinary(binary *BinaryOp) interface{} {
	return p.parenthesize(binary.Operator.Lexeme, binary.Left, binary.Right)
}

func (p *PrettyPrintVisitor) VisitLiteral(literal *LiteralExpr) interface{} {
	if literal.Value == nil {
		return "nil"
	}
	return strconv.Itoa(literal.Value.(int))
}

func (p *PrettyPrintVisitor) VisitUnary(unary *UnaryOp) interface{} {
	return p.parenthesize(unary.Operator.Lexeme, unary.Literal.(Expr))
}

func (p *PrettyPrintVisitor) VisitGroup(group *Group) interface{} {
	return p.parenthesize("group", group.Expression)
}

func (p *PrettyPrintVisitor) parenthesize(name string, expressions ...Expr) string {
	var sb strings.Builder
	_ = sb.WriteByte('(')
	_, _ = sb.WriteString(name)
	for _, expression := range expressions {
		sb.WriteByte(' ')
		sb.WriteString(expression.Accept(p).(string))
	}
	sb.WriteByte(')')
	return sb.String()
}

type Expr interface {
	Accept(visitor AstVisitor) interface{}
}

type BinaryOp struct {
	Left Expr
	Operator Token
	Right Expr
}

func (b *BinaryOp) Accept(visitor AstVisitor) interface{} { return visitor.VisitBinary(b) }

type UnaryOp struct {
	Operator Token
	Literal  Expr
}

func (u *UnaryOp) Accept(visitor AstVisitor) interface{} { return visitor.VisitUnary(u) }

type LiteralExpr struct {
	Value interface{}
}

func (l *LiteralExpr) Accept(visitor AstVisitor) interface{} { return visitor.VisitLiteral(l) }

type Group struct {
	Expression Expr
}

func (g *Group) Accept(visitor AstVisitor) interface{} { return visitor.VisitGroup(g) }